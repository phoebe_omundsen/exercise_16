﻿using System;

namespace exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("*******************************");
            Console.WriteLine("****   Welcome to my App   ****");
            Console.WriteLine("*******************************");
            Console.WriteLine();
            Console.WriteLine("*******************************");
            Console.WriteLine("What is your name?");

            var nameUser = Console.ReadLine();

            Console.WriteLine($"Your name is: {nameUser}");

            Console.ReadKey();

        }
    }
}
